# surf_zone_trees

A wave encounters a group of trees in along a 20 degree slope.  Simulation using two-phase VOF NSE, with Reynolds averaging.  

To make the mesh:
blockMesh
refinemesh -all -overwrite
snappyHexMesh

To Run:

cp -rf 0.orig 0
setFields
interFoam

breaking wave:
![Alt text](surf_trees.png )
return flow:
![Alt text](surf_trees_return.png )
